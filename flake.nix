{
  description = "Generates an emoji list you can use with your wm";

  inputs = {
    naersk.url = "github:nmattia/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    utils,
    naersk,
  }: let
    appName = "emoji-listgen";
    out =
      utils.lib.eachDefaultSystem
      (system: let
        pkgs = import nixpkgs {inherit system;};
        naersk-lib = naersk.lib."${system}";
        nativeBuildInputs = with pkgs; [cargo rustc pkg-config];
        buildInputs = with pkgs; [libressl_3_5];
      in {
        defaultPackage = naersk-lib.buildPackage {
          pname = appName;
          root = builtins.path {
            path = ./.;
            name = "${appName}-src";
          };
          inherit nativeBuildInputs buildInputs;
        };

        defaultApp = utils.lib.mkApp {
          drv = self.defaultPackage.${system};
        };

        devShell = pkgs.mkShell {
          packages =
            nativeBuildInputs
            ++ buildInputs
            ++ (with pkgs; [
              rust-analyzer
              cargo-watch
              clippy
              rustfmt
            ]);
        };
      });
  in
    out
    // {
      overlay = final: prev: {
        ${appName} = self.defaultPackage.${prev.system};
      };
    };
}
