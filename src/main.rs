use select::{
    document::Document,
    predicate::{Class, Name},
};
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    io::{self, Write},
};

fn main() {
    let mut stdout = std::io::stdout();
    gen_emoji_list(&mut stdout);
    stdout.flush().unwrap();
    gen_kaomoji_list(&mut stdout);
    stdout.flush().unwrap();
    eprintln!("all done!");
}

fn write_emoji(writer: &mut impl Write, emoji: &str, description: &str) -> io::Result<()> {
    writeln!(writer, "{} ({})", emoji.trim(), description.trim())
}

fn gen_emoji_list(file: &mut impl Write) {
    eprintln!("downloading emoji list...");
    let raw_html = reqwest::blocking::get("https://unicode.org/emoji/charts/full-emoji-list.html")
        .unwrap()
        .text()
        .unwrap();

    eprintln!("parsing html...");
    let document = Document::from(raw_html.as_str());

    eprintln!("writing emoji...");
    for node in document.find(Name("tr")) {
        let emoji = if let Some(n) = node.find(Class("chars")).next() {
            n.text()
        } else {
            continue;
        };

        let desc = if let Some(n) = node.find(Class("name")).next() {
            n.text()
        } else {
            continue;
        };

        write_emoji(file, &emoji, &desc).unwrap();
    }
    eprintln!("done writing emoji!");
}

fn gen_kaomoji_list(file: &mut impl Write) {
    eprintln!("writing kaomoji...");
    let kaomoji: Kaomoji = serde_yaml::from_str(include_str!("./kaomoji.yaml")).unwrap();
    for (key, value) in kaomoji.kaomoji {
        for (i, art) in value.iter().enumerate() {
            write_emoji(file, art, &format!("{} #{}", &key, i)).unwrap();
        }
    }
}

#[derive(Deserialize, Serialize)]
struct Kaomoji {
    kaomoji: HashMap<String, Vec<String>>,
}
